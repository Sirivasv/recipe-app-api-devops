# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

```
docker-compose build app
docker-compose -f docker-compose-proxy.yml up
```

The API will then be available at http://127.0.0.1:8000

AWS vault command
```
aws-vault exec USERNAME --duration=6h
```

Terraform init
```
docker-compose -f deploy/docker-compose.yml run --rm terraform init
```

Terraform Format config file
```
docker-compose -f deploy/docker-compose.yml run --rm terraform fmt
```

Terraform Validate config
```
docker-compose -f deploy/docker-compose.yml run --rm terraform validate
```

Terraform Plan (Always run before apply!)
```
docker-compose -f deploy/docker-compose.yml run --rm terraform plan
```

Terraform Apply
```
docker-compose -f deploy/docker-compose.yml run --rm terraform apply
```

Terraform Destroy
```
docker-compose -f deploy/docker-compose.yml run --rm terraform destroy
```
Terraform list workspaces
```
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list
```

Terraform create workspace
```
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace new NAME_OF_WORKSPACE
```